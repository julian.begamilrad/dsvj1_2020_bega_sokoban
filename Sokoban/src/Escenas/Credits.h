#ifndef CREDITS_H
#define CREDITS_H

#include<iostream>
#include<fstream> 
#include <sstream>

#include "raylib.h"

#include "Objetos/Player.h"
using namespace std;

namespace ARKANOIDJB
{

	const int SHOWNSCORES = 10;
	class Credits
	{
	public:
		Credits();
		~Credits();
		void cInput();
		void cInit();
		void cDraw();

		static void setPoints(int nPoints);
		static void setTime(int hour, int min, int sec);
		static void setLvl(int nLvl);
	private:
		void loadHighScore();
		void saveHighScore();
		void resetHighScore();
		void refreshHighScore();
		bool alrreadyRefresh;
		bool showHighScores;
		int highScores[SHOWNSCORES];
		static int points;
		static int time[TIMESECTION];
		static int lvl;
	};

}
#endif