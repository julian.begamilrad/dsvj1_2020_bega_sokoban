#include "Menu.h"
namespace ARKANOIDJB
{
	Menu::Menu()
	{
		actualOption = Play;
		Global::setGamestatus(MENU);
		isControlMenu = false;
		select = LoadSound("res/Select.ogg");
		moveSelection = LoadSound("res/MoveSelection.ogg");
		music = LoadMusicStream("res/MenuMusic.mp3");
		menuBackGround = LoadTexture("res/Backgrounds/Menu.png");
		musicOn = true;
	}
	Menu::~Menu()
	{
		UnloadSound(select);     // Unload sound data
		UnloadSound(moveSelection);
		UnloadMusicStream(music);
		UnloadTexture(menuBackGround);
	}
	void Menu::mInit()
	{
		actualOption = Play;
		Global::setGamestatus(MENU);
		isControlMenu = false;
		PlayMusicStream(music);
		musicOn = true;
		
	}
	void Menu::mInput()
	{
		if (IsKeyReleased(KEY_N))
		{
			if (musicOn)PauseMusicStream(music);
			else ResumeMusicStream(music);
			musicOn = !musicOn;
		}
		if (IsKeyReleased(KEY_D) || IsKeyReleased(KEY_RIGHT))
		{
			if (isControlMenu == false)
			{
				PlaySound(moveSelection);

				if (actualOption == Exit)
				{
					actualOption = Play;
				}
				else
				{
					actualOption++;
				}
			}
		}

		if (IsKeyReleased(KEY_A) || IsKeyReleased(KEY_LEFT))
		{
			if (isControlMenu == false)
			{
				PlaySound(moveSelection);
				if (actualOption == Play)
				{
					actualOption = Exit;
				}
				else
				{
					actualOption--;
				}
			}
		}
		if (IsKeyReleased(KEY_ENTER))
		{
			PlaySound(select);
			if (isControlMenu == false)
			{
				switch (actualOption)
				{
				case Play:
					Global::setGamestatus(GAME);
					break;
				case Controls:
					isControlMenu = true;
					break;
				case Credits:
					Global::setGamestatus(CREDITS);
					break;
				case Exit:
					Global::setGamestatus(EXIT);
					break;
				default:
					break;
				}

			}
			else
			{
				isControlMenu = false;
			}
		}
		if (IsKeyDown(KEY_ESCAPE))
		{
			Global::setGamestatus(EXIT);
		}
	}
	void Menu::mUpdate()
	{
		UpdateMusicStream(music);
	}
	void Menu::mDraw()
	{
		Vector2 pos = { -20, -100};
		DrawTextureEx(menuBackGround, pos, 0, 1.05, WHITE);
		if (isControlMenu == false)
		{
			DrawText("Arkanoid", GetScreenWidth() / HALFDIVIDER - MeasureText("Arkanoid", 80) / HALFDIVIDER, GetScreenHeight() / 8 - 25, 80, LIGHTGRAY);
			DrawText("Arkanoid version 1 . 2", GetScreenWidth() / HALFDIVIDER - MeasureText("Arkanoid version 1 . 2", TextSize / HALFDIVIDER) / HALFDIVIDER, (GetScreenHeight() / HALFDIVIDER) + (TextSize * 7), TextSize / HALFDIVIDER, LIGHTGRAY);
			DrawText("Play", DISTANCEDEFAULT * TEXTDISTANCEINX*2, DISTANCEDEFAULT * CREDITSTEXTDISTANCEINY, DISTANCEDEFAULT, WHITE);
			DrawText("Controls", DISTANCEDEFAULT * TEXTDISTANCEINX*4, DISTANCEDEFAULT * CREDITSTEXTDISTANCEINY, DISTANCEDEFAULT, WHITE);
			DrawText("Credits", DISTANCEDEFAULT * TEXTDISTANCEINX*7, DISTANCEDEFAULT * CREDITSTEXTDISTANCEINY, DISTANCEDEFAULT, WHITE);
			DrawText("Exit", DISTANCEDEFAULT * TEXTDISTANCEINX*9.5f, DISTANCEDEFAULT * CREDITSTEXTDISTANCEINY, DISTANCEDEFAULT, WHITE);

			switch (actualOption)
			{
			case Play:
				DrawText("Play", DISTANCEDEFAULT * TEXTDISTANCEINX*2, DISTANCEDEFAULT * CREDITSTEXTDISTANCEINY, DISTANCEDEFAULT, YELLOW);
				break;

			case Controls:
				DrawText("Controls", DISTANCEDEFAULT * TEXTDISTANCEINX * 4, DISTANCEDEFAULT * CREDITSTEXTDISTANCEINY, DISTANCEDEFAULT, YELLOW);
				break;

			case Credits:
				DrawText("Credits", DISTANCEDEFAULT * TEXTDISTANCEINX * 7, DISTANCEDEFAULT * CREDITSTEXTDISTANCEINY, DISTANCEDEFAULT, YELLOW);
				break;

			case Exit:
				DrawText("Exit", DISTANCEDEFAULT * TEXTDISTANCEINX * 9.5f, DISTANCEDEFAULT * CREDITSTEXTDISTANCEINY, DISTANCEDEFAULT, YELLOW);
				break;
			default:
				break;
			}
			

		}
		if (isControlMenu == true)
		{
			DrawText("Controls", GetScreenWidth() / HALFDIVIDER - MeasureText("Controls", 80) / HALFDIVIDER, GetScreenHeight() / 8 - 25, 80, GRAY);
			DrawText("Press enter to go back to main menu", (GetScreenWidth() / HALFDIVIDER) - (MeasureText("Press enter to go back to main menu", TextSize) / HALFDIVIDER), (GetScreenHeight() / HALFDIVIDER) + (TextSize * 6), TextSize, LIGHTGRAY);

			DrawRectangle(GetScreenWidth() / 7, (GetScreenHeight() / HALFDIVIDER) - WIDTH, WIDTH, WIDTH, WHITE);
			DrawText("A", GetScreenWidth() / 7 + 15, (GetScreenHeight() / HALFDIVIDER) - WIDTH, WIDTH, BLACK);
			DrawText("Move left", GetScreenWidth() / QUARTERDIVIDER, (GetScreenHeight() / HALFDIVIDER) - (WIDTH/8)*6, WIDTH/HALFDIVIDER, YELLOW);


			
			DrawRectangle(GetScreenWidth() / 7, (GetScreenHeight() / HALFDIVIDER) + WIDTH, WIDTH, WIDTH, WHITE);
			DrawText("D", GetScreenWidth() / 7 + 15, (GetScreenHeight() / HALFDIVIDER) + WIDTH, WIDTH, BLACK);
			DrawText("Move right", GetScreenWidth() / QUARTERDIVIDER, (GetScreenHeight() / HALFDIVIDER) + WIDTH + ((WIDTH / 4) ), WIDTH / HALFDIVIDER, YELLOW);


			DrawRectangle((GetScreenWidth() / 8) * 5, (GetScreenHeight() / HALFDIVIDER) - WIDTH, WIDTH, WIDTH, WHITE);
			DrawText("N", (GetScreenWidth() / 8) * 5 + 15, (GetScreenHeight() / HALFDIVIDER) - WIDTH, WIDTH, BLACK);
			DrawText("no music", (GetScreenWidth() / 8) * 6, (GetScreenHeight() / HALFDIVIDER) - (WIDTH / 8) * 6, WIDTH / HALFDIVIDER, YELLOW);


			DrawRectangle((GetScreenWidth() / 8) * 5, (GetScreenHeight() / HALFDIVIDER) + WIDTH, WIDTH, WIDTH, WHITE);
			DrawText("P", (GetScreenWidth() / 8) * 5 + 15, (GetScreenHeight() / HALFDIVIDER) + WIDTH, WIDTH, BLACK);
			DrawText("Pause", (GetScreenWidth() / 8) * 6, (GetScreenHeight() / HALFDIVIDER) + WIDTH + ((WIDTH / 4)), WIDTH / HALFDIVIDER, YELLOW);


		}
	}

}