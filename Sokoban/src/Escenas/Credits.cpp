#include "Credits.h"

namespace ARKANOIDJB
{
	int Credits::points = STARTDEFAULTNUMBER;
	int Credits::time[] = { STARTDEFAULTNUMBER,STARTDEFAULTNUMBER,STARTDEFAULTNUMBER };
	int Credits::lvl = STARTDEFAULTNUMBER;
	Credits::Credits()
	{
		highScores[0] = STARTDEFAULTNUMBER;
		highScores[1] = STARTDEFAULTNUMBER;
		highScores[2] = STARTDEFAULTNUMBER;
		highScores[3] = STARTDEFAULTNUMBER;
		highScores[4] = STARTDEFAULTNUMBER;
		highScores[5] = STARTDEFAULTNUMBER;
		highScores[6] = STARTDEFAULTNUMBER;
		highScores[7] = STARTDEFAULTNUMBER;
		highScores[8] = STARTDEFAULTNUMBER;
		highScores[9] = STARTDEFAULTNUMBER;

		
		showHighScores = false;
		alrreadyRefresh = false;
	}
	Credits::~Credits()
	{
	}
	void Credits::cInput()
	{
		if (IsKeyReleased(KEY_M))
		{
			if (showHighScores == false)
			{
			Global::setGamestatus(MENU);
			loadHighScore();
			refreshHighScore();
			saveHighScore();
			alrreadyRefresh = false;
			points = STARTDEFAULTNUMBER;
			lvl = STARTDEFAULTNUMBER;
			time[0] = STARTDEFAULTNUMBER;
			time[1] = STARTDEFAULTNUMBER;
			time[2] = STARTDEFAULTNUMBER;
			}
			else
			{
				showHighScores = !showHighScores;
			}
		}
		if (IsKeyReleased(KEY_ESCAPE))
		{
			Global::setGamestatus(MENU);
			cInit();
		}
		if (IsKeyReleased(KEY_ENTER))
		{
			showHighScores = !showHighScores;
			loadHighScore();
			refreshHighScore();
			saveHighScore();

		}
#if DEBUG
		{
			if (IsKeyReleased(KEY_S))
			{
				resetHighScore();
			}
			if (IsKeyReleased(KEY_T))
			{
				for (int i = 0; i < 10; i++)
				{
					cout << highScores[i] << endl;
				}
			}
			if (IsKeyReleased(KEY_R))
			{
				refreshHighScore();
			}
			if (IsKeyReleased(KEY_L))
			{
				loadHighScore();
			}
		}
#endif
	}
	void Credits::cInit()
	{
		for (int i = 0; i < 10; i++)
		{
			highScores[i] = 0;
		}
		showHighScores = false;
		alrreadyRefresh = false;
	}
	void Credits::cDraw()
	{
		if (showHighScores == false)
		{
			DrawText(TextFormat("Points %1i", points), GetScreenWidth() / HALFDIVIDER - MeasureText("Created by Julian Bega", TextSize) , GetScreenHeight() / HALFDIVIDER - (TextSize * 4), TextSize, LIGHTGRAY);
			DrawText(TextFormat("TIME %1i", time[1]), GetScreenWidth() / HALFDIVIDER - MeasureText("TIME 100 :", 40) / HALFDIVIDER, GetScreenHeight() / HALFDIVIDER - (TextSize * 4), 25, LIGHTGRAY);
			DrawText(TextFormat(": %1i", time[2]), GetScreenWidth() / HALFDIVIDER - MeasureText(".", 40) / 2, GetScreenHeight() / HALFDIVIDER - (TextSize * 4), 25, LIGHTGRAY);
			DrawText(TextFormat("lvl %1i", lvl), GetScreenWidth() - GetScreenWidth() / 5, GetScreenHeight() / HALFDIVIDER - (TextSize * 4), 25, LIGHTGRAY);
			DrawText("Pres Enter to see the top 10 scores", GetScreenWidth() / HALFDIVIDER - MeasureText("Pres H to see the top 10 scores", TextSize) / 2, GetScreenHeight() / HALFDIVIDER - static_cast<int>(TextSize * 1.7), TextSize, YELLOW);
			DrawText("Created by Julian Bega", GetScreenWidth() / HALFDIVIDER - MeasureText("Created by Julian Bega", TextSize) / 2, GetScreenHeight() / HALFDIVIDER, TextSize, LIGHTGRAY);
			DrawText("Materia Practica profesional 1, TSDV", GetScreenWidth() / HALFDIVIDER - MeasureText("Materia Practica profesional 1, TSDV", TextSize) / HALFDIVIDER, (GetScreenHeight() / HALFDIVIDER) + TextSize, TextSize, LIGHTGRAY);
			DrawText("Using Raylib", GetScreenWidth() / HALFDIVIDER - MeasureText("Using Raylib", TextSize) / HALFDIVIDER, (GetScreenHeight() / HALFDIVIDER) + (TextSize * DOUBLE), TextSize, LIGHTGRAY);
			DrawText("Press M to go back to menu", GetScreenWidth() / HALFDIVIDER - MeasureText("Press Enter/Escape to go back to menu", TextSize) / HALFDIVIDER, (GetScreenHeight() / HALFDIVIDER) + (TextSize * TRIPLE), TextSize, LIGHTGRAY);
			DrawText("Sound effects obtained from https ://www.zapsplat.com", GetScreenWidth() / HALFDIVIDER - MeasureText("Sound effects obtained from https ://www.zapsplat.com", TextSize / HALFDIVIDER) / HALFDIVIDER, (GetScreenHeight() / HALFDIVIDER) + static_cast<int>(TextSize * 4.3), TextSize / HALFDIVIDER, LIGHTGRAY);
			DrawText("Textures obtained from https://craftpix.net/file-licenses/", GetScreenWidth() / HALFDIVIDER - MeasureText("Textures obtained from https://craftpix.net/file-licenses/", TextSize / HALFDIVIDER) / HALFDIVIDER, (GetScreenHeight() / HALFDIVIDER) + (TextSize * 5), TextSize / HALFDIVIDER, LIGHTGRAY);
			DrawText("Arkanoid version 1 . 2", GetScreenWidth() / HALFDIVIDER - MeasureText("Arkanoid version 1 . 2", TextSize / HALFDIVIDER) / HALFDIVIDER, (GetScreenHeight() / HALFDIVIDER) + (TextSize * 7), TextSize / HALFDIVIDER, LIGHTGRAY);
		}
		else
		{
			DrawText("HighScores", 500 , 40, 40, GRAY);
			DrawText("Red is your score", 515, 90, 20, GRAY);
			DrawText("Your scores this game", 515, 125, 20, GRAY);
			DrawText(TextFormat("Points %1i", points), 515, 160, 20, GRAY);
			DrawText(TextFormat("TIME   %1i", time[1]), 515, 190, 20, GRAY);
			DrawText(TextFormat(": %1i", time[2]), 615, 190, 20, GRAY);
			DrawText(TextFormat("lvl %1i", lvl), 515, 220, 20, GRAY);
			for (int i = 0; i < 10; i++)
			{
				DrawText(TextFormat("- %1i", i+1), 50, 50 + (40 * i), 25, WHITE);
				DrawText(TextFormat("Points: %1i", highScores[i]), 100, 50 + (40*i), 25, WHITE);

			}
			DrawText(TextFormat("- %1i", 1), 50, 50 , 25, YELLOW);
			DrawText(TextFormat("Points: %1i", highScores[0]), 100, 50, 25, YELLOW);
			DrawText(TextFormat("- %1i", 2), 50, 50 + (40), 25, GRAY);
			DrawText(TextFormat("Points: %1i", highScores[1]), 100, 50 + (40), 25, GRAY);
			for (int i = 0; i < 10; i++)
			{
				if (points == highScores[i])
				{
					DrawText(TextFormat("- %1i", i + 1), 50, 50 + (40 * i), 25, RED);
					DrawText(TextFormat("Points: %1i", highScores[i]), 100, 50 + (40 * i), 25, RED);
					i = i + 10;
				}				
			}

		}
	}

	void Credits::setPoints(int nPoints)
	{
		points = nPoints;
	}

	void Credits::setTime(int hour, int min, int sec)
	{
		time[0] = hour;
		time[1] = min;
		time[2] = sec;
	}

	void Credits::setLvl(int nLvl)
	{
		lvl = nLvl;
	}


	void Credits::loadHighScore()
	{
		int i = 0;
		char line[20] = "";
		ifstream FileScoreEnt;
		FileScoreEnt.exceptions(ifstream::failbit | ifstream::badbit);
		try {
			FileScoreEnt.open("HighScores.txt", ios::in | ios::app);
			cout << "abriendo el archivo Scores" << endl;
			try {
				cout << "leyendo todo el archivo Scores" << endl;
				FileScoreEnt.getline(line, 20);
				while (!FileScoreEnt.eof()) {
					highScores[i] = stoi(line);
					cout << line << endl;
					FileScoreEnt.getline(line, 20);
					i++;
				}
				cout << line << endl;
			}
			catch (ifstream::failure& e) {
				if (FileScoreEnt.bad())
					cout << "hubo un error al leer el archivo Scores";
			}
			try { FileScoreEnt.close(); cout << "cerrando el archivo Scores" << endl; }
			catch (ifstream::failure& e) { if (FileScoreEnt.fail())cout << "Hubo un error al cerrar el archivo Scores"; }
		}
		catch (ifstream::failure& e) { if (FileScoreEnt.fail())cout << "Hubo un error al abrir el archivo Scores"; }
		catch (...) {
			cout << "error desconocido" << endl;
		}
	}

	void Credits::saveHighScore()
	{
		char cadena[80] = "";
		ofstream fexit;
		fexit.open("HighScores.txt", ios::out);
		for (int i = 0; i < 10; i++)
		{
			fexit << highScores[i] << endl;
		}

		fexit.close();
	}
	void Credits::resetHighScore()
	{
		char cadena[80] = "";
		ofstream fexit;
		fexit.open("HighScores.txt", ios::out);
		for (int i = 0; i < 10; i++)
		{
			fexit << 0 << endl;
		}

		fexit.close();
	}

	void Credits::refreshHighScore()
	{
		if (alrreadyRefresh == false)
		{
			for (int i = 0; i < 10; i++)
			{
				if (highScores[i] < points)
				{
					for (int j = 9; j >= i + 1; j--)
					{
						highScores[j] = highScores[j - 1];
					}
					highScores[i] = points;
					i = 10;
				}
			}
			alrreadyRefresh = true;
		}
	}


}