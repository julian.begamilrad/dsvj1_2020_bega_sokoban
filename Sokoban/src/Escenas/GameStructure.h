#ifndef GAMESTRUCTURE_H
#define GAMESTRUCTURE_H

#include "Global.h"
#include "Game.h"
#include "Menu.h"
#include "Credits.h"

namespace ARKANOIDJB
{
	class GameStructure
	{
	public:

		GameStructure();
		~GameStructure();
		void gameLoop();
		void gsInit();
		void gsInput();
		void gsUpdate();
		void setConfig();
		void gsDraw();
		void gsDeInit();
		const int screenWidth = 810;
		const int screenHeight = 450;

		Game* game;
		Menu* menu;
		Credits* credits;
		bool firstTime;

	private:
		bool inGame;

	};

}
#endif