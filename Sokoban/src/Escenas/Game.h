#ifndef GAME_H
#define GAME_H

#include "raylib.h"

#include "Objetos/Player.h"
#include "Objetos/Ball.h"
#include "Objetos/Brick.h"
#include "Credits.h"

namespace ARKANOIDJB
{
	enum POWERUPS
	{
		LIFEUP, GROWPADDLE, GROWPADDLE2 , PADDLESPEED, PADDLESPEED2
	};
	const int bricksCol = 8;
	const int bricksRow = 5;
	const int STARTDEFAULTLVL = 1;
	class Game
	{
	public:

		Game();
		~Game();
		void gInit();
		void gInput();
		void gUpdate();
		void gDraw();	

	private:
		void gameTime();
		void powerUp();

		Player player;
		Ball ball;
		Brick bricks[bricksCol][bricksRow];

		int lvl;
		bool allBricksBroken;
		bool inGame;
		bool pause;
		float seconds;
		int timer[TIMESECTION];
		Music music;
		Sound clearLvl;
		Sound noHP;
		Sound sPowerUp;
		Sound hit;
		bool musicOn;

		int tryGetPowerUp;
		int lastTryGetPowerUp;

		Texture2D blueBrick;
		Texture2D orangeBrick;
		Texture2D greenBrick;
		Texture2D pinkBrick;
		Texture2D purpleBrick;

		Texture2D playerTexture;
		Texture2D ePlayerTexture;
		Texture2D ballTexture;
		Texture2D background;
	};

}

#endif