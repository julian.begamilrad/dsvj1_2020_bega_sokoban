#include "Global.h"
namespace ARKANOIDJB
{
	int Global::gamestatus = MENU;
	int Global::lastGamestatus = MENU;
	Global::Global()
	{
		gamestatus = MENU;
		lastGamestatus = MENU;
	}

	Global::~Global()
	{
	}
	int Global::getGamestatus()
	{
		return gamestatus;
	}
	int Global::getLastGamestatus()
	{
		return lastGamestatus;
	}
	void Global::setGamestatus(int ngstatus)
	{
		gamestatus = ngstatus;
	}
	void Global::setLastGamestatus(int nlgstatus)
	{
		lastGamestatus = nlgstatus;
	}
}